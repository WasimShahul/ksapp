package com.wasim.ksapp.service;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.wasim.ksapp.MainActivity;

/**
 * Created by wasim on 12/03/18.
 */

public class ClosingService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

//        Toast.makeText(this, "Closing Service", Toast.LENGTH_SHORT).show();

        MainActivity mainActivity = new MainActivity();
        mainActivity.finishActivity();

        // Handle application closing
//        ((MainActivity) getBaseContext()).finishActivity();

        // Destroy the service
        this.stopSelf();
    }
}