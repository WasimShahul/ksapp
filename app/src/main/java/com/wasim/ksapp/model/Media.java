package com.wasim.ksapp.model;

public class Media {
    public String idSender;
    public String idReceiver;
    public String text;
    public boolean isPortrait;
    public String imageUrl;
    public String gifUrl;
    public String videoUrl;
    public long timestamp;
}