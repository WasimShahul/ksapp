package com.wasim.ksapp.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiImageView;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.EmojiTextView;
import com.vanniktech.emoji.emoji.Emoji;
import com.vanniktech.emoji.ios.IosEmojiProvider;
import com.vanniktech.emoji.listeners.OnEmojiBackspaceClickListener;
import com.vanniktech.emoji.listeners.OnEmojiClickListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardCloseListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardOpenListener;
import com.vlk.multimager.activities.GalleryActivity;
import com.vlk.multimager.utils.Constants;
import com.vlk.multimager.utils.Image;
import com.vlk.multimager.utils.Params;
import com.wasim.ksapp.R;
import com.wasim.ksapp.data.SharedPreferenceHelper;
import com.wasim.ksapp.data.StaticConfig;
import com.wasim.ksapp.model.Consersation;
import com.wasim.ksapp.model.Message;
import com.wasim.ksapp.util.FontFaces;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ChatActivity";
    private RecyclerView recyclerChat;
    public static final int VIEW_TYPE_USER_MESSAGE = 0;
    public static final int VIEW_TYPE_FRIEND_MESSAGE = 1;
    private ListMessageAdapter adapter;
    private String roomId;
    TextView friendName, isTypingTV;
    ViewGroup rootView;
    private ImageView smileyBtn, gifBtn, cameraBtn;
    private ArrayList<CharSequence> idFriend;
    private Consersation consersation;
    private ImageButton btnSend;
    private EmojiEditText editWriteMessage;
    private LinearLayoutManager linearLayoutManager;
    public static HashMap<String, Bitmap> bitmapAvataFriend;
    public Bitmap bitmapAvataUser;
    EmojiPopup emojiPopup;
    private SlidingUpPanelLayout mLayout;
    private StorageReference mStorageRef;
    private CardView onlineCard;
    private ProgressBar uploadingProgressBar;
    private RelativeLayout progressLayout;
    private ImageView uploadingImg;
    private TextView progresstxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EmojiManager.install(new IosEmojiProvider());
        setContentView(R.layout.activity_chat);

        Window window = getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimaryDark));

        friendName = (TextView) findViewById(R.id.friendName);
        isTypingTV = (TextView) findViewById(R.id.isTypingTV);
        rootView = findViewById(R.id.sliding_layout);
        Intent intentData = getIntent();
        idFriend = intentData.getCharSequenceArrayListExtra(StaticConfig.INTENT_KEY_CHAT_ID);
        roomId = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_ROOM_ID);
        String nameFriend = intentData.getStringExtra(StaticConfig.INTENT_KEY_CHAT_FRIEND);

        Log.e(TAG, "FriendId " + idFriend);
        Log.e(TAG, "roomId " + roomId);
        Log.e(TAG, "nameFriend " + nameFriend);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        consersation = new Consersation();
        btnSend = (ImageButton) findViewById(R.id.btnSend);
        smileyBtn = (ImageView) findViewById(R.id.smileyBtn);
        gifBtn = (ImageView) findViewById(R.id.gifBtn);
        cameraBtn = (ImageView) findViewById(R.id.cameraBtn);
        editWriteMessage = (EmojiEditText) findViewById(R.id.editWriteMessage);
        uploadingProgressBar = (ProgressBar) findViewById(R.id.uploadingProgressBar);
        progressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
        uploadingImg = (ImageView) findViewById(R.id.uploadingImg);
        progresstxtView = (TextView) findViewById(R.id.progresstxtView);
        progresstxtView.setTypeface(FontFaces.robotoLight(this));
        progressLayout.setVisibility(View.GONE);
        editWriteMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isTyping(true);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    isTyping(false);
                } else {
                    isTyping(false);
                }
            }
        });
        onlineCard = (CardView) findViewById(R.id.onlineCard);

//        Toast.makeText(ChatActivity.this, "MyId" + StaticConfig.UID, Toast.LENGTH_SHORT).show();

        FirebaseDatabase.getInstance().getReference().child("user").child(String.valueOf(idFriend.get(0))).child("status").child("isTyping").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    boolean isTyping = (Boolean) dataSnapshot.getValue();
                    if (isTyping) {
                        isTypingTV.setVisibility(View.VISIBLE);
                    } else {
                        isTypingTV.setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID).child("status").child("isOnline").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isOnline = (Boolean) dataSnapshot.getValue();
                if (isOnline) {
                    onlineCard.setVisibility(View.VISIBLE);
                } else {
                    onlineCard.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });

        gifBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayout.setAnchorPoint(0.7f);
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatActivity.this, GalleryActivity.class);
                Params params = new Params();
                params.setCaptureLimit(10);
                params.setPickerLimit(10);
//                params.setToolbarColor(selectedColor);
//                params.setActionButtonColor(selectedColor);
//                params.setButtonTextColor(selectedColor);
                intent.putExtra(Constants.KEY_PARAMS, params);
                startActivityForResult(intent, Constants.TYPE_MULTI_PICKER);
            }
        });

        btnSend.setOnClickListener(this);
        smileyBtn.setOnClickListener(this);
        editWriteMessage.setOnClickListener(this);
        friendName.setTypeface(FontFaces.robotomedium(ChatActivity.this));
        friendName.setText(nameFriend);
        String base64AvataUser = SharedPreferenceHelper.getInstance(this).getUserInfo().avata;
        if (!base64AvataUser.equals(StaticConfig.STR_DEFAULT_BASE64)) {
            byte[] decodedString = Base64.decode(base64AvataUser, Base64.DEFAULT);
            bitmapAvataUser = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } else {
            bitmapAvataUser = null;
        }

        emojiPopup = EmojiPopup.Builder.fromRootView(rootView)
                .setOnEmojiBackspaceClickListener(new OnEmojiBackspaceClickListener() {
                    @Override
                    public void onEmojiBackspaceClick(View v) {

                    }
                })
                .setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
                    @Override
                    public void onEmojiPopupShown() {
                        smileyBtn.setImageResource(R.drawable.keyboard);
                    }
                })
                .setOnEmojiClickListener(new OnEmojiClickListener() {
                    @Override
                    public void onEmojiClick(@NonNull EmojiImageView emoji, @NonNull Emoji imageView) {

                    }
                })
                .setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
                    @Override
                    public void onEmojiPopupDismiss() {
                        smileyBtn.setImageResource(R.drawable.smiley);
                    }
                })
                .setOnSoftKeyboardOpenListener(new OnSoftKeyboardOpenListener() {
                    @Override
                    public void onKeyboardOpen(int keyBoardHeight) {

                    }
                })
                .setOnSoftKeyboardCloseListener(new OnSoftKeyboardCloseListener() {
                    @Override
                    public void onKeyboardClose() {

                    }
                })
                .build(editWriteMessage);

        editWriteMessage.setTypeface(FontFaces.robotoLight(ChatActivity.this));
        if (idFriend != null && nameFriend != null) {
//            getSupportActionBar().setTitle(nameFriend);
            linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerChat = (RecyclerView) findViewById(R.id.recyclerChat);
            recyclerChat.setLayoutManager(linearLayoutManager);
            adapter = new ListMessageAdapter(this, consersation, bitmapAvataFriend, bitmapAvataUser);
            FirebaseDatabase.getInstance().getReference().child("message/" + roomId).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.getValue() != null) {
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                        Message newMessage = new Message();
                        newMessage.idSender = (String) mapMessage.get("idSender");
                        newMessage.idReceiver = (String) mapMessage.get("idReceiver");
                        newMessage.text = (String) mapMessage.get("text");
                        newMessage.imageUrl = (String) mapMessage.get("imageUrl");
                        newMessage.timestamp = (long) mapMessage.get("timestamp");
                        consersation.getListMessageData().add(newMessage);
                        adapter.notifyDataSetChanged();
                        linearLayoutManager.scrollToPosition(consersation.getListMessageData().size() - 1);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            recyclerChat.setAdapter(adapter);
        }
    }

    public void isTyping(boolean val) {
        FirebaseDatabase.getInstance().getReference().child("user").child(StaticConfig.UID).child("status").child("isTyping").setValue(val);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent result = new Intent();
            result.putExtra("idFriend", idFriend.get(0));
            setResult(RESULT_OK, result);
            this.finish();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent result = new Intent();
        result.putExtra("idFriend", idFriend.get(0));
        setResult(RESULT_OK, result);
        this.finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSend) {
            String content = editWriteMessage.getText().toString().trim();
            if (content.length() > 0) {
                editWriteMessage.setText("");
                Message newMessage = new Message();
                newMessage.text = content;
                newMessage.idSender = StaticConfig.UID;
                newMessage.idReceiver = roomId;
                newMessage.timestamp = System.currentTimeMillis();
                FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage);
            }
        }
        if (view.getId() == R.id.smileyBtn) {
            emojiPopup.toggle();

        }
    }

    private void getGif() {
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        RequestParams params = new RequestParams();
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get("https://api.tenor.com/v1/search?q=romance&key=KTXX1FVNEBC2&limit=5&anon_id=3a76e56901d740da9e59ffb22b988242", params, new GetMyGifResponsehandler());
    }

    public class GetMyGifResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
//            Gif deals = new Gson().fromJson(new String(responseBody), Gif.class);
//            if (deals.success == 1) {
//                Log.e("Categories", "Array "+deals.categoriesList);
//                categoriesAdapter = new CategoriesAdapter(deals.categoriesList, CategoriesListActivity.this);
//                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(CategoriesListActivity.this, 1);
//                rv_categories.setLayoutManager(mLayoutManager);
//                rv_categories.setAdapter(categoriesAdapter);
//            } else if(deals.error_code == 450){
//                Utils utils = new Utils();
//                utils.refreshToken(CategoriesListActivity.this);
//
//                getCategories();
//            }else if(deals.error_code == 449){
//
//            }
//            else{
////                Toast.makeText(CategoriesListActivity.this, "Something wrong", Toast.LENGTH_LONG).show();
//            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
//            Toast.makeText(CategoriesListActivity.this,"Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    private void insertImageMessage(String imageUrl, boolean isPortrait , String imageName) {
        String content = "";
        editWriteMessage.setText("");
        Message newMessage = new Message();
        newMessage.text = content;
        newMessage.idSender = StaticConfig.UID;
        newMessage.idReceiver = roomId;
        newMessage.imageUrl = imageUrl;
        newMessage.imageName = imageName;
        newMessage.isPortrait = isPortrait;
        newMessage.timestamp = System.currentTimeMillis();
        FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            Toast.makeText(this, "NOt OK", Toast.LENGTH_SHORT).show();
            return;
        }
        switch (requestCode) {
            case Constants.TYPE_MULTI_CAPTURE:
                ArrayList<Image> capturedImagesList = intent.getParcelableArrayListExtra(Constants.KEY_BUNDLE_LIST);
                for (Image image :
                        capturedImagesList) {
                    Uri file = Uri.fromFile(new File(image.imagePath));
                    final String imageName = StaticConfig.UID + System.currentTimeMillis();
                    final boolean isPortrait = image.isPortraitImage;
                    StorageReference riversRef = mStorageRef.child(StaticConfig.UID + "/" + imageName);

                    riversRef.putFile(file)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // Get a URL to the uploaded content
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    insertImageMessage(downloadUrl + "", isPortrait, imageName);
//                                    Toast.makeText(ChatActivity.this, downloadUrl+"", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                    // ...
//                                    Toast.makeText(ChatActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
                break;
            case Constants.TYPE_MULTI_PICKER:
                ArrayList<Image> pickedImagesList = intent.getParcelableArrayListExtra(Constants.KEY_BUNDLE_LIST);
                for (final Image image :
                        pickedImagesList) {
                    final Uri file = Uri.fromFile(new File(image.imagePath));
                    final String imageName = StaticConfig.UID + System.currentTimeMillis();
                    final boolean isPortrait = image.isPortraitImage;
                    StorageReference riversRef = mStorageRef.child(StaticConfig.UID + "/" + imageName);

                    riversRef.putFile(file)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // Get a URL to the uploaded content
                                    progressLayout.setVisibility(View.GONE);
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    insertImageMessage(downloadUrl + "", isPortrait, imageName);
//                                    Toast.makeText(ChatActivity.this, downloadUrl+"", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    progressLayout.setVisibility(View.GONE);
                                    // Handle unsuccessful uploads
                                    // ...
//                                    Toast.makeText(ChatActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            Glide.with(ChatActivity.this).load(file).into(uploadingImg);
                            progressLayout.setVisibility(View.VISIBLE);
                            Log.e("ChatActivity", "Bytes uploaded: " + taskSnapshot.getBytesTransferred());
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            uploadingProgressBar.setProgress((int) progress);
                            progresstxtView.setText((int) progress+"%");
                            //displaying percentage in progress dialog
//                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");

                        }
                    });
                }
                break;
        }
    }

}

class ListMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Consersation consersation;
    private HashMap<String, Bitmap> bitmapAvata;
    private HashMap<String, DatabaseReference> bitmapAvataDB;
    private Bitmap bitmapAvataUser;

    public ListMessageAdapter(Context context, Consersation consersation, HashMap<String, Bitmap> bitmapAvata, Bitmap bitmapAvataUser) {
        this.context = context;
        this.consersation = consersation;
        this.bitmapAvata = bitmapAvata;
        this.bitmapAvataUser = bitmapAvataUser;
        bitmapAvataDB = new HashMap<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ChatActivity.VIEW_TYPE_FRIEND_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.rc_item_message_friend, parent, false);
            return new ItemMessageFriendHolder(view);
        } else if (viewType == ChatActivity.VIEW_TYPE_USER_MESSAGE) {
            View view = LayoutInflater.from(context).inflate(R.layout.rc_item_message_user, parent, false);
            return new ItemMessageUserHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemMessageFriendHolder) {
            ((ItemMessageFriendHolder) holder).txtContent.setTypeface(FontFaces.robotoRegular(context));
            boolean isPortrait = consersation.getListMessageData().get(position).isPortrait;
            String imageUrl = consersation.getListMessageData().get(position).imageUrl;
            String imageName = consersation.getListMessageData().get(position).imageName;
            if (imageUrl != null) {
                ((ItemMessageFriendHolder) holder).contentImage.setVisibility(View.VISIBLE);
                ((ItemMessageFriendHolder) holder).contentImageCard.setVisibility(View.VISIBLE);
//                ((ItemMessageFriendHolder) holder).txtContent.setText(imageName);
                if (isPortrait) {
                    Glide.with(context).load(imageUrl).asGif().placeholder(R.drawable.loading).into(((ItemMessageFriendHolder) holder).contentImage);
                } else {
                    Glide.with(context).load(imageUrl).asGif().placeholder(R.drawable.loading).into(((ItemMessageFriendHolder) holder).contentImage);
                }

            } else {
                ((ItemMessageFriendHolder) holder).contentImage.setVisibility(View.GONE);
                ((ItemMessageFriendHolder) holder).contentImageCard.setVisibility(View.GONE);
                ((ItemMessageFriendHolder) holder).txtContent.setText(consersation.getListMessageData().get(position).text);
            }

//            String text = ((ItemMessageFriendHolder) holder).txtContent.getText().toString();
//            final EmojiInformation emojiInformation = EmojiUtils.emojiInformation(text);
//            final int res;
//
//            if (emojiInformation.isOnlyEmojis && emojiInformation.emojis.size() == 1) {
//                res = R.dimen.emoji_size_single_emoji;
//            } else if (emojiInformation.isOnlyEmojis && emojiInformation.emojis.size() > 1) {
//                res = R.dimen.emoji_size_only_emojis;
//            } else {
//                res = R.dimen.emoji_size_default;
//            }
//
//            ((ItemMessageFriendHolder) holder).txtContent.setEmojiSize(res, false);

            try {
                Bitmap currentAvata = bitmapAvata.get(consersation.getListMessageData().get(position).idSender);
                if (currentAvata != null) {
                    ((ItemMessageFriendHolder) holder).avata.setImageBitmap(currentAvata);

                } else {
                    final String id = consersation.getListMessageData().get(position).idSender;
                    if (bitmapAvataDB.get(id) == null) {
                        bitmapAvataDB.put(id, FirebaseDatabase.getInstance().getReference().child("user/" + id + "/avata"));
                        bitmapAvataDB.get(id).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.getValue() != null) {
                                    String avataStr = (String) dataSnapshot.getValue();
                                    if (!avataStr.equals(StaticConfig.STR_DEFAULT_BASE64)) {
                                        byte[] decodedString = Base64.decode(avataStr, Base64.DEFAULT);
                                        ChatActivity.bitmapAvataFriend.put(id, BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length));
                                    } else {
                                        ChatActivity.bitmapAvataFriend.put(id, BitmapFactory.decodeResource(context.getResources(), R.drawable.default_avata));
                                    }
                                    notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }
            } catch (Exception e) {

            }

        } else if (holder instanceof ItemMessageUserHolder) {
            ((ItemMessageUserHolder) holder).txtContent.setTypeface(FontFaces.robotoRegular(context));
            ((ItemMessageUserHolder) holder).txtContent.setText(consersation.getListMessageData().get(position).text);
            String imageUrl = consersation.getListMessageData().get(position).imageUrl;
            boolean isPortrait = consersation.getListMessageData().get(position).isPortrait;
            if (imageUrl != null) {
                ((ItemMessageUserHolder) holder).contentImage.setVisibility(View.VISIBLE);
                ((ItemMessageUserHolder) holder).contentImageCard.setVisibility(View.VISIBLE);
                if (isPortrait) {
                    Glide.with(context).load(imageUrl).into(((ItemMessageUserHolder) holder).contentImage);
                } else {
                    Glide.with(context).load(imageUrl).into(((ItemMessageUserHolder) holder).contentImage);
                }
            } else {
                ((ItemMessageUserHolder) holder).contentImage.setVisibility(View.GONE);
                ((ItemMessageUserHolder) holder).contentImageCard.setVisibility(View.GONE);
            }
//            String text = ((ItemMessageUserHolder) holder).txtContent.getText().toString();
//            final EmojiInformation emojiInformation = EmojiUtils.emojiInformation(text);
//            final int res;
//
//            if (emojiInformation.isOnlyEmojis && emojiInformation.emojis.size() == 1) {
//                res = R.dimen.emoji_size_single_emoji;
//            } else if (emojiInformation.isOnlyEmojis && emojiInformation.emojis.size() > 1) {
//                res = R.dimen.emoji_size_only_emojis;
//            } else {
//                res = R.dimen.emoji_size_default;
//            }
//
//            ((ItemMessageUserHolder) holder).txtContent.setEmojiSize(res, false);

            if (bitmapAvataUser != null) {
                ((ItemMessageUserHolder) holder).avata.setImageBitmap(bitmapAvataUser);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return consersation.getListMessageData().get(position).idSender.equals(StaticConfig.UID) ? ChatActivity.VIEW_TYPE_USER_MESSAGE : ChatActivity.VIEW_TYPE_FRIEND_MESSAGE;
    }

    @Override
    public int getItemCount() {
        return consersation.getListMessageData().size();
    }
}

class ItemMessageUserHolder extends RecyclerView.ViewHolder {
    public EmojiTextView txtContent;
    public CircleImageView avata;
    public ImageView contentImage;
    public CardView contentImageCard;

    public ItemMessageUserHolder(View itemView) {
        super(itemView);
        txtContent = (EmojiTextView) itemView.findViewById(R.id.textContentUser);
        avata = (CircleImageView) itemView.findViewById(R.id.imageView2);
        contentImage = (ImageView) itemView.findViewById(R.id.contentImage);
        contentImageCard = (CardView) itemView.findViewById(R.id.contentImageCard);
    }
}

class ItemMessageFriendHolder extends RecyclerView.ViewHolder {
    public EmojiTextView txtContent;
    public CircleImageView avata;
    public ImageView contentImage;
    public CardView contentImageCard;

    public ItemMessageFriendHolder(View itemView) {
        super(itemView);
        txtContent = (EmojiTextView) itemView.findViewById(R.id.textContentFriend);
        avata = (CircleImageView) itemView.findViewById(R.id.imageView3);
        contentImage = (ImageView) itemView.findViewById(R.id.contentImage);
        contentImageCard = (CardView) itemView.findViewById(R.id.contentImageCard);
    }
}
