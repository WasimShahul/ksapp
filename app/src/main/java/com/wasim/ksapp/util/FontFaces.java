package com.wasim.ksapp.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by DELL5547 on 14-Jul-17.
 */

public class FontFaces {

    public static Typeface robotoItalic(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotoitalic.ttf");
    }

    public static Typeface robotoBlackItalic(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotoblackitalic.ttf");
    }

    public static Typeface robotoBlack(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotoblack.ttf");
    }

    public static Typeface robotoBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotobold.ttf");
    }

    public static Typeface robotoRegular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotoregular.ttf");
    }

    public static Typeface robotoLight(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotolight.ttf");
    }

    public static Typeface robotothin(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotothin.ttf");
    }

    public static Typeface robotomedium(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "robotomedium.ttf");
    }
}
